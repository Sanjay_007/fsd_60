package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;


@WebServlet("/UpdateEmployee")
public class UpdateEmployee extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int empId = Integer.parseInt(request.getParameter("empId"));
		String empName = request.getParameter("empName");
		String gender = request.getParameter("gender");
		double salary = Double.parseDouble(request.getParameter("salary"));
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		EmployeeDao employeeDao = new EmployeeDao();
		int result = employeeDao.updateEmployee(empId, empName, gender, salary, emailId, password);
		
		if(result > 0 ){
			request.getRequestDispatcher("GetAllEmployees").forward(request, response);
		}else{
			request.getRequestDispatcher("GetAllEmployees.jsp").include(request, response);
			out.print("<br/>");
			out.print("<h3 style = 'color:red;'>Unable to Update Employee Record!!!</h3>");
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
