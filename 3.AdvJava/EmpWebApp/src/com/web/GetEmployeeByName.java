package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;


@WebServlet("/GetEmployeeByName")
public class GetEmployeeByName extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String empName = request.getParameter("empName");
		EmployeeDao employeeDao = new EmployeeDao();
		Employee employee = employeeDao.getEmployeeByName(empName);
			
		out.print("<body bgcolor='lightyellow' text='green'>");
		
			
		if (employee != null) {			
				
			//adding the employeee object under request object
			request.setAttribute("employee", employee);
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("GetEmployeeByName.jsp");
			requestDispatcher.include(request, response);
		} else {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("HRHomePage.jsp");
			requestDispatcher.include(request, response);
			
			out.print("<h1 style='color:red'>Unable to Fetch Employee Records</h1>");
		}
		out.print("</body>");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
