import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

  transform(value: any, value2: any): any {      
    return (value2 == 'Male')? 'Mr.'+ value : 'Miss.'+value; 
  }

}
