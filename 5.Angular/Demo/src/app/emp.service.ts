import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  loginStatus: boolean;

  constructor(private http : HttpClient) {          //Dependency Injection for HttpClient
    this.loginStatus = false;
  }

  //Fetching all countries from rest countries api
  getAllCountries():any{
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  getAllEmployees():any{
    return this.http.get('http://localhost:8085/getAllEmployees');
  }

  getEmployeeById(empId:any): any{
    return this.http.get('http://localhost:8085/getEmployeeById/' + empId);
  }

  getAllDepartments(): any{
    return this.http.get('http://localhost:8085/getAllDepartments');
  }

  registerEmployee(emp: any){
    return this.http.post('http://localhost:8085/addEmployee',emp);
  }

  getAllProducts():any{
    return this.http.get('http://localhost:8085/getAllProducts');
  }

  updateEmployee(emp: any){
    return this.http.put('http://localhost:8085/updateEmployee', emp);
  }

  deleteEmployee(empId:any) {
    return this.http.delete('http://localhost:8085/deleteEmployeeById/' +empId);
  }

  employeeLogin(emailId:any, password:any){
    return this.http.get('http://localhost:8085/empLogin/' + emailId +'/'+ password).toPromise();
  }

  isUserLoggedIn() {
    this.loginStatus = true;
  }

  isUserLoggedOut(){
    this.loginStatus = false;
  }

  getLoginStatus(): boolean{
    return this.loginStatus;
  }
}
