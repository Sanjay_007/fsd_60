import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrl: './showempbyid.component.css'
})
export class ShowempbyidComponent {
  employees:any;
  empId:number;
  emp:any;
  emailId:any;

  constructor(private service: EmpService){
    this.empId = 0;

    // fetching emailId from local Storage
    this.emailId = localStorage.getItem('emailId');
    // this.employees = [
    //   {empId:101, empName:'Harsha', salary:1222.22, gender:'Male', doj:'11-13-2018',country:'IND', emailId:'harha@gmail.com', password:'123'},
    //   {empId:102, empName:'Sasuke', salary:2424.24, gender:'Male', doj:'10-19-2019',country:'CHINA', emailId:'sasuke@gmail.com', password:'123'},
    //   {empId:103, empName:'Indra', salary:3434.34, gender:'Male', doj:'01-23-2020',country:'JAPAN', emailId:'indra@gmail.com', password:'123'},
    //   {empId:104, empName:'Madara', salary:5454.54, gender:'Male', doj:'05-03-2021',country:'JAPAN', emailId:'madara@gmail.com', password:'123'},
    //   {empId:105, empName:'Itachi', salary:7878.78, gender:'Male', doj:'08-22-2012',country:'LEAF Village', emailId:'itachi@gmail.com', password:'123'},
    //   {empId:106, empName:'Naruto', salary:6778.78, gender:'Male', doj:'10-22-2019',country:'LEAF', emailId:'naruto@gmail.com', password:'123'},
    //   {empId:107, empName:'Sakura', salary:778.78, gender:'female', doj:'10-22-2018',country:'LEAF', emailId:'sakura@gmail.com', password:'123'},
    // ]
  }

  getEmployee(){
    this.emp = null;
    
    this.service.getEmployeeById(this.empId).subscribe((data:any) =>{
      console.log(data);
      this.emp = data;
    })
  }

}
