package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Employee;

@Service
public class EmployeeDao {

	@Autowired
	EmployeeRepository employeeRepository;

	public List<Employee> getAllEmployees() {
		return employeeRepository.findAll();
	}

	public Employee getEmployeeById(int empId) {
		return employeeRepository.findById(empId).orElse(null);
	}

	public List<Employee> getEmployeeByName(String eName) {
		return employeeRepository.findByName(eName);
	}

	public Employee addEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	public void deleteEmployeeById(int empId) {
		employeeRepository.deleteById(empId);;
	}

	public Employee empLogin(String emailId, String password) {
		return employeeRepository.empLogin(emailId, password);
	}
}
