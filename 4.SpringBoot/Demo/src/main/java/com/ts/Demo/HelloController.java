package com.ts.Demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@RequestMapping("sayHello")
	public String sayHello(){
		
		return "<h1>Hello World!.....sayHello of HelloController.....</h1>";
	}
	
	@RequestMapping("sayWelcome")
	public String sayWelcome(){
		return "Welcome from HelloController...";
	}
	
}
