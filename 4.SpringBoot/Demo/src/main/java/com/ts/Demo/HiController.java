package com.ts.Demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiController {

	@RequestMapping("sayHi")
	public String sayHi(){
		return "HI From HiController"; 
	}
	
	@RequestMapping("welcome")
	public String welcome(){
		return "welome From HiController";
	}
}
