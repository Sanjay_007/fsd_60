package day01;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;

public class SelectDemo {
	public static void main(String[] args){
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		
		String selectQuery = "select*from employee";
		
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectQuery);
			if(resultSet != null){
				while(resultSet.next()){
					System.out.println("EmpID : "+ resultSet.getInt(1));
					System.out.println("EmpNAme : "+ resultSet.getString(2));
					System.out.println("Salary : "+ resultSet.getDouble(3));
					System.out.println("Gender : "+ resultSet.getString(4));
					System.out.println("EmailID : "+ resultSet.getString(5));
					System.out.println("PAssword : "+ resultSet.getString(6));
					System.out.println();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if(connection != null){
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
